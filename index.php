<?php
require 'vendor/autoload.php';

//instancie o objeto
$app = new \Slim\Slim();
$t = new \teste\Teste();
//defina a rota
$app->get('/hello/:name', function ($name) { 
  echo "Hello, $name!"; 
}); 
//rode a aplicação Slim 
$app->run();